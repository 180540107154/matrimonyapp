package com.example.matrimonyapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    CardView cvregistration,cvuserlist,cvsearch,cvfavourite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViewReferance();
        onClickRegiestrationForm();
        onClickUserList();
        onClickSearch();
        onClickFavourite();
    }

    void initViewReferance(){
        cvregistration = findViewById(R.id.cvRegistration);
        cvuserlist = findViewById(R.id.cvList);
        cvsearch = findViewById(R.id.cvSearch);
        cvfavourite = findViewById(R.id.cvFavourite);
    }

    void onClickRegiestrationForm(){
        cvregistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,UserListRegistration.class);
                startActivity(intent);
            }
        });
    }

    void onClickUserList(){
        cvuserlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,ActivityUserListByGender.class);
                startActivity(intent);
            }
        });
    }

    void onClickSearch(){
        cvsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,ActivitySearchUser.class);
                startActivity(intent);
            }
        });
    }

    void onClickFavourite(){
        cvfavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,ActivityFavouriteUserlist.class);
                startActivity(intent);
            }
        });
    }
}
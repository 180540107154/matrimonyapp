package com.example.matrimonyapp;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.matrimonyapp.adapter.UserListAdapter;
import com.example.matrimonyapp.database.TblUser;
import com.example.matrimonyapp.model.UserModel;
import com.example.matrimonyapp.util.Constant;

import java.util.ArrayList;

public class ActivityFavouriteUserlist extends AppCompatActivity {

    RecyclerView rcvFavouriteUserList;
    TextView NoFavouriteUserFound;

    ArrayList<UserModel> userlist = new ArrayList<>();
    UserListAdapter adapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.favourite_userlist);
        initViewReference();
        setAdapter();
        checkAndVisibleView();
    }

    void setAdapter(){
        rcvFavouriteUserList.setLayoutManager(new GridLayoutManager(this,1));
        userlist.addAll(new TblUser(this).getFavouriteUserlist());
        adapter = new UserListAdapter(this, userlist, new UserListAdapter.OnViewClickListener() {
            @Override
            public void OnDeleteClick(int position) {
                showAlertDialog(position);
            }

            @Override
            public void OnItemClicked(int position) {
                Intent intent = new Intent(ActivityFavouriteUserlist.this, UserDetail.class);
                intent.putExtra(Constant.USER_OBJECT,userlist.get(position));
                startActivity(intent);
            }

            @Override
            public void OnFavouriteClick(int position) {
                int lastUpdatedFavouriteStatus = new TblUser(ActivityFavouriteUserlist.this).updateFavouriteStatus( 0,userlist.get(position).getUserId());
                if(lastUpdatedFavouriteStatus > 0 ){
                   userlist.remove(position);
                   adapter.notifyItemRemoved(position);
                   adapter.notifyItemRangeChanged(0,userlist.size());
                    checkAndVisibleView();
                }
            }
        });
        rcvFavouriteUserList.setAdapter(adapter);

    }

    void showAlertDialog(int position) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Are You Sure Want To Delete This User?");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                (dialog, which) -> {
                    int deletedUserId = new TblUser(this).deleteUserById(userlist.get(position).getUserId());
                    if (deletedUserId > 0) {
                        Toast.makeText(this, "Deleted Successfully", Toast.LENGTH_SHORT).show();
                        userlist.remove(position);
                        adapter.notifyItemRemoved(position);
                        adapter.notifyItemRangeChanged(0, userlist.size());
                        checkAndVisibleView();
                    } else {
                        Toast.makeText(this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                (dialog, which) -> dialog.dismiss());
        alertDialog.show();
    }



    void initViewReference(){
        rcvFavouriteUserList = findViewById(R.id.rcvFavouriteUserList);
        NoFavouriteUserFound = findViewById(R.id.tvNoFavouriteUserFound);
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(ActivityFavouriteUserlist.this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    void checkAndVisibleView() {
        if (userlist.size() > 0) {
            Log.d("if", "checkAndVisibleView: " + userlist.size());
            NoFavouriteUserFound.setVisibility(View.GONE);
            rcvFavouriteUserList.setVisibility(View.VISIBLE);
        }else {
            Log.d("else if", "checkAndVisibleView: " + userlist.size());
            NoFavouriteUserFound.setVisibility(View.VISIBLE);
            rcvFavouriteUserList.setVisibility(View.GONE);
        }
    }
}

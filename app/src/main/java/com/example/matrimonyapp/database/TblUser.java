package com.example.matrimonyapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.matrimonyapp.model.UserModel;
import com.example.matrimonyapp.util.Utils;

import java.util.ArrayList;

public class TblUser extends MyDatabase {

    public  static  final  String TABLE_NAME = "TblUser";
    public  static  final  String USER_ID = "UserId";
    public  static  final  String FIRST_NAME = "FirstName";
    public  static  final  String MIDDLE_NAME = "MiddleName";
    public  static  final  String LAST_NAME = "LastName";
    public  static  final  String MOBILE_NUMBER = "MobileNumber";
    public  static  final  String EMAIL_ADDRESS = "Email";
    public  static  final  String GENDER = "Gender";
    public  static  final  String HOBBIES = "Hobbies";
    public  static  final  String DATE_OF_BIRTH = "Dob";
    public  static  final  String CITY_ID = "CityId";
    public  static  final  String EDUCATION_ID = "EducationId";
    public  static  final  String LANGUAGE_ID = "LanguageId";
    public  static  final  String IS_FAVOURITE = "IsFavourite";

    /* Query COlUMN */
    public  static  final  String EDUCATION = "EducationName";
    public  static  final  String LANGUAGE = "LanguageName";
    public  static  final  String CITY = "CityName";

    public TblUser(Context context) {
        super(context);
    }

    public ArrayList<UserModel> getUserList(){
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserModel> list = new ArrayList<>();
        String query =
                " SELECT " +
                " UserId, " +
                " FirstName, " +
                " MiddleName, " +
                " LastName, " +
                " MobileNumber, " +
                " Email, " +
                " Gender, " +
                " Hobbies, " +
                " Dob, "+
                " TblMstCity.CityId, " +
                " TblMstEducation.EducationId, " +
                " TblMstLanguage.LanguageId , " +
                " TblMstLanguage.LanguageName, " +
                " TblMstCity.CityName , " +
                " TblMstEducation.EducationName , " +
                " IsFavourite " +
                " FROM "+
                 " TBLUSER " +
                " INNER JOIN TBLMSTLANGUAGE ON TBLUSER.LanguageId = TBLMSTLANGUAGE.LanguageId " +
                " INNER JOIN TBLMSTCITY ON TBLUSER.CityId = TBLMSTCITY.CityId " +
                " INNER JOIN TBLMSTEDUCATION ON TBLUSER.EducationId = TBLMSTEDUCATION.EducationId ";
        Cursor cursor = db.rawQuery(query,null);
        cursor.moveToFirst();
        for (int i = 0 ; i<cursor.getCount();i++){
            list.add(getCreatedModelUsingCursor(cursor));
            cursor.moveToNext();

        }
        cursor.close();
        db.close();
        return list;
    }

    public UserModel getCreatedModelUsingCursor(Cursor cursor){
        UserModel model = new UserModel();
        model.setUserId(cursor.getInt(cursor.getColumnIndex(USER_ID)));
        model.setCityId(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
        model.setLanguageId(cursor.getInt(cursor.getColumnIndex(LANGUAGE_ID)));
        model.setEducationId(cursor.getInt(cursor.getColumnIndex(EDUCATION_ID)));
        model.setIsFavourite(cursor.getInt(cursor.getColumnIndex(IS_FAVOURITE)));
        model.setFirstName(cursor.getString(cursor.getColumnIndex(FIRST_NAME)));
        model.setMiddleName(cursor.getString(cursor.getColumnIndex(MIDDLE_NAME)));
        model.setLastName(cursor.getString(cursor.getColumnIndex(LAST_NAME)));
        model.setMobileNumber(cursor.getString(cursor.getColumnIndex(MOBILE_NUMBER)));
        model.setEmail(cursor.getString(cursor.getColumnIndex(EMAIL_ADDRESS)));
        model.setGender(cursor.getInt(cursor.getColumnIndex(GENDER)));
        model.setHobbies(cursor.getString(cursor.getColumnIndex(HOBBIES)));
        model.setDob(Utils.getDateToDisplay(cursor.getString(cursor.getColumnIndex(DATE_OF_BIRTH))));
        model.setLanguage(cursor.getString(cursor.getColumnIndex(LANGUAGE)));
        model.setCity(cursor.getString(cursor.getColumnIndex(CITY)));
        model.setEducation(cursor.getString(cursor.getColumnIndex(EDUCATION)));
        return  model;
    }

    public UserModel getUserById(int id){
        SQLiteDatabase db = getReadableDatabase();
        UserModel model = new UserModel();
        String query = " SELECT * FROM " + TABLE_NAME  + " WHERE " + USER_ID + " = ? ";
        Cursor cursor = db.rawQuery(query,new String[]{String.valueOf(id)});
        cursor.moveToFirst();
        model = getCreatedModelUsingCursor(cursor);
        cursor.close();
        db.close();
        return model;

    }

    public ArrayList<UserModel> getUserlistByGender(int gender){
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserModel> list = new ArrayList<>();
        String query =
                " SELECT " +
                " UserId, " +
                " FirstName, " +
                " MiddleName, " +
                " LastName, " +
                " MobileNumber, " +
                " Email, " +
                " Gender, " +
                " Hobbies, " +
                " Dob, "+
                " TblMstCity.CityId, " +
                " TblMstEducation.EducationId, " +
                " TblMstLanguage.LanguageId , " +
                " TblMstLanguage.LanguageName, " +
                " TblMstCity.CityName , " +
                " TblMstEducation.EducationName , " +
                " IsFavourite " +
                " FROM "+
                " TBLUSER " +
                " INNER JOIN TBLMSTLANGUAGE ON TBLUSER.LanguageId = TBLMSTLANGUAGE.LanguageId " +
                " INNER JOIN TBLMSTCITY ON TBLUSER.CityId = TBLMSTCITY.CityId " +
                " INNER JOIN TBLMSTEDUCATION ON TBLUSER.EducationId = TBLMSTEDUCATION.EducationId " + " WHERE " + GENDER + " = ? ";
        Cursor cursor = db.rawQuery(query,new String[]{String.valueOf(gender)});
        cursor.moveToFirst();
        for (int i = 0 ; i<cursor.getCount();i++){
            list.add(getCreatedModelUsingCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return  list;
    }

    public ArrayList<UserModel> getFavouriteUserlist(){
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserModel> list = new ArrayList<>();
        String query =
                " SELECT " +
                        " UserId, " +
                        " FirstName, " +
                        " MiddleName, " +
                        " LastName, " +
                        " MobileNumber, " +
                        " Email, " +
                        " Gender, " +
                        " Hobbies, " +
                        " Dob, "+
                        " TblMstCity.CityId, " +
                        " TblMstEducation.EducationId, " +
                        " TblMstLanguage.LanguageId , " +
                        " TblMstLanguage.LanguageName, " +
                        " TblMstCity.CityName , " +
                        " TblMstEducation.EducationName , " +
                        " IsFavourite " +
                        " FROM "+
                        " TBLUSER " +
                        " INNER JOIN TBLMSTLANGUAGE ON TBLUSER.LanguageId = TBLMSTLANGUAGE.LanguageId " +
                        " INNER JOIN TBLMSTCITY ON TBLUSER.CityId = TBLMSTCITY.CityId " +
                        " INNER JOIN TBLMSTEDUCATION ON TBLUSER.EducationId = TBLMSTEDUCATION.EducationId " + " WHERE " +
                        IS_FAVOURITE + " = ? ";
        Cursor cursor = db.rawQuery(query,new String[]{String.valueOf(1)});
        cursor.moveToFirst();
        for (int i = 0 ; i<cursor.getCount();i++){
            list.add(getCreatedModelUsingCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return  list;
    }

    public long insertUserData(String FirstName,
                               String MiddleName,
                               String LastName ,
                               String MobileNumber ,
                               String EmailAddress,
                               int Gender,
                               String Hobbies,
                               String Dob,
                               int CityId,
                               int EducationId,
                               int LanguageId,
                               int is_Favourite){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(FIRST_NAME,FirstName);
        cv.put(MIDDLE_NAME,MiddleName);
        cv.put(LAST_NAME,LastName);
        cv.put(MOBILE_NUMBER,MobileNumber);
        cv.put(EMAIL_ADDRESS,EmailAddress);
        cv.put(GENDER,Gender);
        cv.put(HOBBIES,Hobbies);
        cv.put(DATE_OF_BIRTH,Dob);
        cv.put(CITY_ID,CityId);
        cv.put(EDUCATION_ID,EducationId);
        cv.put(LANGUAGE_ID,LanguageId);
        cv.put(IS_FAVOURITE,is_Favourite);
        long lastInsertedID = db.insert(TABLE_NAME, null, cv);
        db.close();
        return lastInsertedID;

    }

    public int updateUserData(String FirstName,
                               String MiddleName,
                               String LastName ,
                               String MobileNumber ,
                               String EmailAddress,
                               int Gender,
                               String Hobbies,
                               String Dob,
                               int CityId,
                               int EducationId,
                               int LanguageId,
                               int is_Favourite,
                               int userId){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(FIRST_NAME,FirstName);
        cv.put(MIDDLE_NAME,MiddleName);
        cv.put(LAST_NAME,LastName);
        cv.put(MOBILE_NUMBER,MobileNumber);
        cv.put(EMAIL_ADDRESS,EmailAddress);
        cv.put(GENDER,Gender);
        cv.put(HOBBIES,Hobbies);
        cv.put(DATE_OF_BIRTH,Dob);
        cv.put(CITY_ID,CityId);
        cv.put(EDUCATION_ID,EducationId);
        cv.put(LANGUAGE_ID,LanguageId);
        cv.put(IS_FAVOURITE,is_Favourite);
        int lastUpdatedId = db.update(TABLE_NAME, cv,USER_ID + " = ? ",new String[]{String.valueOf(userId)});
        db.close();
        return lastUpdatedId;

    }

    public  int deleteUserById(int userId){
        SQLiteDatabase db = getWritableDatabase();
        int deletedUserId = db.delete(TABLE_NAME,USER_ID + " = ? ",new String[]{String.valueOf(userId)});
        db.close();
        return deletedUserId;
    }

    public  int updateFavouriteStatus(int isFavourite,int userId){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(IS_FAVOURITE,isFavourite);
        int lastUpdatedId = db.update(TABLE_NAME, cv,USER_ID + " = ? ",new String[]{String.valueOf(userId)});
        db.close();
        return lastUpdatedId;
    }
}

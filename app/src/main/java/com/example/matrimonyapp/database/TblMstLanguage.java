package com.example.matrimonyapp.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.matrimonyapp.model.EducationModel;
import com.example.matrimonyapp.model.LanguageModel;

import java.util.ArrayList;

public class TblMstLanguage extends MyDatabase {

    public  static  final  String TBLMSTLANGUAGE = "TblMstLanguage";
    public  static  final  String LANGUAGE_ID = "LanguageId";
    public  static  final  String  LANGUAGE_NAME = "LanguageName";

    public TblMstLanguage(Context context) {
        super(context);
    }


    public ArrayList<LanguageModel> getLanguagelist(){
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<LanguageModel> list = new ArrayList<>();
        String query = "SELECT * FROM " + TBLMSTLANGUAGE;
        Cursor cursor = db.rawQuery(query,null);
        cursor.moveToFirst();
        LanguageModel languageModel1 = new LanguageModel();
        languageModel1.setLanguageName("Select One");
        list.add(0,languageModel1);
        for (int i = 0 ; i<cursor.getCount();i++){
            LanguageModel languageModel = new LanguageModel();
            languageModel.setLanguageId(cursor.getInt(cursor.getColumnIndex(LANGUAGE_ID)));
            languageModel.setLanguageName(cursor.getString(cursor.getColumnIndex(LANGUAGE_NAME)));
            list.add(languageModel);
            cursor.moveToNext();
        }

        cursor.close();
        db.close();

        return list;
    }
}

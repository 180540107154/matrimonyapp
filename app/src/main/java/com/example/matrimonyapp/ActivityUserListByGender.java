package com.example.matrimonyapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.example.matrimonyapp.adapter.GenderViewPagerAdapter;
import com.google.android.material.tabs.TabLayout;

import static androidx.fragment.app.FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT;

public class ActivityUserListByGender extends AppCompatActivity {

    TabLayout tabgender;
    ViewPager vpuserlist;
    ImageView ivfavouriteuserlist;

    GenderViewPagerAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gender_wise_list);
        initViewReference();
        setUpViewPageAdapter();
        onClickFavouriteUserList();
    }

    void  initViewReference(){
        tabgender = findViewById(R.id.tabGender);
        vpuserlist = findViewById(R.id.vpUserList);
        ivfavouriteuserlist = findViewById(R.id.ivFavouriteUsersScreen);
    }

    void onClickFavouriteUserList(){
        ivfavouriteuserlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityUserListByGender.this,ActivityFavouriteUserlist.class);
                startActivity(intent);
            }
        });

    }

    void setUpViewPageAdapter(){
        adapter = new GenderViewPagerAdapter(getSupportFragmentManager(),BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT,this);
        vpuserlist.setAdapter(adapter);
        tabgender.setupWithViewPager(vpuserlist);
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(ActivityUserListByGender.this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }
}

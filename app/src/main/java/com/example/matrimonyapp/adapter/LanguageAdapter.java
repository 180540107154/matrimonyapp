package com.example.matrimonyapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.example.matrimonyapp.R;
import com.example.matrimonyapp.model.LanguageModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LanguageAdapter extends BaseAdapter {
    Context context;
    ArrayList<LanguageModel> languagelist;

    public LanguageAdapter(Context context, ArrayList<LanguageModel> languagelist) {
        this.context = context;
        this.languagelist = languagelist;
    }

    @Override
    public int getCount() {
        return languagelist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewgroup) {
            View v = view;
            LanguageAdapter.ViewHolder viewHolder;
            if (v== null){
                v = LayoutInflater.from(context).inflate(R.layout.view_row_text,null);
                viewHolder = new ViewHolder(v);
                v.setTag(viewHolder);
            }else{
                viewHolder = (LanguageAdapter.ViewHolder) v.getTag();

            }
            viewHolder.tvSpinnerList.setText(languagelist.get(i).getLanguageName());

            if(i==0){
                viewHolder.tvSpinnerList.setTextColor(ContextCompat.getColor(context,R.color.gray));
            }else{
                viewHolder.tvSpinnerList.setTextColor(ContextCompat.getColor(context,R.color.black));
            }
            return v;
    }

    @Override
    public boolean isEnabled(int i) {
        if (i == 0) {
            return false;
        } else {
            return true;
        }
    }

    static
    class ViewHolder {
        @BindView(R.id.tvSpinnerList)
        TextView tvSpinnerList;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}

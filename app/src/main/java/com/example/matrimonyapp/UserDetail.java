package com.example.matrimonyapp;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.matrimonyapp.model.UserModel;
import com.example.matrimonyapp.util.Constant;
import com.example.matrimonyapp.util.Utils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UserDetail extends AppCompatActivity {

    @BindView(R.id.tvActTitleName)
    TextView tvActTitleName;
    @BindView(R.id.tvActFullName)
    TextView tvActFullName;
    @BindView(R.id.tvActMobileNumber)
    TextView tvActMobileNumber;
    @BindView(R.id.tvActEmailAddress)
    TextView tvActEmailAddress;
    @BindView(R.id.tvActAge)
    TextView tvActAge;
    @BindView(R.id.tvActDob)
    TextView tvActDob;
    @BindView(R.id.tvActCity)
    TextView tvActCity;
    @BindView(R.id.tvActLanguage)
    TextView tvActLanguage;
    @BindView(R.id.tvActEducation)
    TextView tvActEducation;
    @BindView(R.id.tvActGender)
    TextView tvActGender;
    @BindView(R.id.tvActHobby)
    TextView tvActHobby;
    @BindView(R.id.flbtnEdit)
    FloatingActionButton flbtnEdit;

    @BindView(R.id.ivGenderAvatar)
    ImageView ivGenderAvatar;


    UserModel userModel;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_datail);
        ButterKnife.bind(this);
        displayUserDetail();
    }

    void displayUserDetail() {
        if (getIntent().hasExtra(Constant.USER_OBJECT)) {
            userModel = (UserModel) getIntent().getSerializableExtra(Constant.USER_OBJECT);
            ivGenderAvatar.setImageResource(userModel.getGender() == Constant.MALE ? R.drawable.ic_groom_1 : R.drawable.ic_woman);
            tvActTitleName.setText(userModel.getFirstName() + " " + userModel.getLastName());
            tvActFullName.setText(userModel.getFirstName() + " " + userModel.getMiddleName() + " " + userModel.getLastName());
            tvActMobileNumber.setText(userModel.getMobileNumber());
            tvActEmailAddress.setText(userModel.getEmail());
            String dateParts[] = userModel.getDob().split("/");
            String age = Utils.getAge(Integer.parseInt(dateParts[2]), Integer.parseInt(dateParts[1]), Integer.parseInt(dateParts[0]));
            Log.d("::1::", "onBindViewHolder: " + dateParts[2] + dateParts[1] + dateParts[0]);
            tvActAge.setText(age);
            tvActDob.setText(userModel.getDob());
            tvActCity.setText(userModel.getCity());
            tvActLanguage.setText(userModel.getLanguage());
            tvActEducation.setText(userModel.getEducation());
            tvActGender.setText(userModel.getGender() == Constant.MALE ? Constant.MALE_DISPLAY : Constant.FEMALE_DISPLAY);
            tvActHobby.setText(userModel.getHobbies());
        }
    }

    @OnClick(R.id.flbtnEdit)
    public void onClick() {
        Intent intent = new Intent(this,UserListRegistration.class);
        intent.putExtra(Constant.USER_OBJECT,userModel);
        startActivity(intent);
    }
}

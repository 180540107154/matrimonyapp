package com.example.matrimonyapp;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.matrimonyapp.adapter.CityAdapter;
import com.example.matrimonyapp.adapter.EducationAdapter;
import com.example.matrimonyapp.adapter.LanguageAdapter;
import com.example.matrimonyapp.database.TblMstCity;
import com.example.matrimonyapp.database.TblMstEducation;
import com.example.matrimonyapp.database.TblMstLanguage;
import com.example.matrimonyapp.database.TblUser;
import com.example.matrimonyapp.model.CityModel;
import com.example.matrimonyapp.model.EducationModel;
import com.example.matrimonyapp.model.LanguageModel;
import com.example.matrimonyapp.model.UserModel;
import com.example.matrimonyapp.util.Constant;
import com.example.matrimonyapp.util.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class UserListRegistration extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    EditText etdob,etFirstname,etMiddlename,etLastName,etMobileNumber,etEmail;
    String stratingdate = "2000-01-01T11:49:00";
    CityAdapter cityAdapter;
    LanguageAdapter languageAdapter;
    EducationAdapter educationAdapter;
    Spinner spcity,splanguage,speducation;
    RadioGroup rgGender;
    RadioButton rbFemale,rbMale;
    CheckBox cbCricket,cbFootball,cbHockey,cbBasketball;
    Button btnsubmit;
    TextView tvTitle;


    ArrayList<CityModel> citylist = new ArrayList<>();
    ArrayList<LanguageModel> languagelist = new ArrayList<>();
    ArrayList<EducationModel>  educationlist = new ArrayList<>();

    UserModel userModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_form);
        viewByRefernce();
        setDateToView();
        setSpinner();
        getDataforUpdate();
        onDobClick();
        btnSubmitOnClick();

    }

    void getDataforUpdate(){
        if (getIntent().hasExtra(Constant.USER_OBJECT)){
            userModel = (UserModel) getIntent().getSerializableExtra(Constant.USER_OBJECT);
            tvTitle.setText(R.string.edit_user_title_lbl);
            etFirstname.setText(userModel.getFirstName());
            etMiddlename.setText(userModel.getMiddleName());
            etLastName.setText(userModel.getLastName());
            etMobileNumber.setText(userModel.getMobileNumber());
            etEmail.setText(userModel.getEmail());
            etdob.setText(userModel.getDob());
            spcity.setSelection(userModel.getCityId(),true);
            splanguage.setSelection(userModel.getLanguageId(),true);
            speducation.setSelection(userModel.getEducationId(),true);

            if(userModel.getGender() == Constant.MALE){
                rbMale.setChecked(true);
            }else {
                rbFemale.setChecked(true);
            }

            String hobbySubPart[] = userModel.getHobbies().split(",");
            for (int i = 0;i<hobbySubPart.length;i++){
                if (hobbySubPart[i].equals(Constant.CRICKET)){
                    cbCricket.setChecked(true);
                    continue;
                }
                if (hobbySubPart[i].equals(Constant.BASKETBALL)){
                    cbBasketball.setChecked(true);
                    continue;
                }
                if (hobbySubPart[i].equals(Constant.FOOTBALL)){
                    cbFootball.setChecked(true);
                    continue;
                }
                if (hobbySubPart[i].equals(Constant.HOCKEY)){
                    cbHockey.setChecked(true);
                    continue;
                }
            }
        }
    }

    void setDate(){
        if (userModel == null){
            final Calendar newCalendar = Calendar.getInstance();
            Date date = Utils.getDatefromString(stratingdate);
            newCalendar.setTimeInMillis(date.getTime());
            DatePickerDialog datePickerDialog = new DatePickerDialog(
                    UserListRegistration.this, UserListRegistration.this, newCalendar.get(Calendar.YEAR),
                    newCalendar.get(Calendar.MONTH),
                    newCalendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.show();
        }else {
            final Calendar newCalendar = Calendar.getInstance();
            Date date = Utils.getDatefromStringUpdated(userModel.getDob());
            newCalendar.setTimeInMillis(date.getTime());
            DatePickerDialog datePickerDialog = new DatePickerDialog(
                    UserListRegistration.this, UserListRegistration.this, newCalendar.get(Calendar.YEAR),
                    newCalendar.get(Calendar.MONTH),
                    newCalendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.show();
        }

    }

    void onDobClick(){
        etdob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDate();
            }
        });
    }

    void btnSubmitOnClick(){
        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isValid()){

                    if(userModel == null){
                        String Hobby = "";
                        if (cbCricket.isChecked()) {
                            Hobby += "," + cbCricket.getText().toString();
                        }
                        if (cbFootball.isChecked()) {
                            Hobby += "," + cbFootball.getText().toString();
                        }
                        if (cbHockey.isChecked()) {
                            Hobby += "," + cbHockey.getText().toString();
                        }
                        if(cbBasketball.isChecked()){
                            Hobby += "," + cbBasketball.getText().toString();
                        }

                        Hobby = Hobby.substring(1);

                        long lastinertedid = new TblUser(getApplicationContext()).insertUserData(
                                etFirstname.getText().toString(),
                                etMiddlename.getText().toString(),
                                etLastName.getText().toString(),
                                etMobileNumber.getText().toString(),
                                etEmail.getText().toString(),
                                rbMale.isChecked()?Constant.MALE:Constant.FEMALE,
                                Hobby,
                                Utils.getFormatedDateToInserted(etdob.getText().toString()),
                                citylist.get(spcity.getSelectedItemPosition()).getCityId(),
                                languagelist.get(splanguage.getSelectedItemPosition()).getLanguageId(),
                                educationlist.get(speducation.getSelectedItemPosition()).getEducationId(),
                                0);

                        if (lastinertedid > 0) {
                            Toast.makeText(UserListRegistration.this, "User Inserted Successfully", Toast.LENGTH_SHORT).show();
                            Intent addCandidateIntent = new Intent(UserListRegistration.this, ActivityUserListByGender.class);
                            startActivity(addCandidateIntent);
                        } else {
                            Toast.makeText(UserListRegistration.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        String Hobby = "";
                        if (cbCricket.isChecked()) {
                            Hobby += "," + cbCricket.getText().toString();
                        }
                        if (cbFootball.isChecked()) {
                            Hobby += "," + cbFootball.getText().toString();
                        }
                        if (cbHockey.isChecked()) {
                            Hobby += "," + cbHockey.getText().toString();
                        }
                        if(cbBasketball.isChecked()){
                            Hobby += "," + cbBasketball.getText().toString();
                        }

                        Hobby = Hobby.substring(1);

                        long lastinertedid = new TblUser(getApplicationContext()).updateUserData(etFirstname.getText().toString().trim(),
                                etMiddlename.getText().toString().trim(),
                                etLastName.getText().toString().trim(),
                                etMobileNumber.getText().toString().trim(),
                                etEmail.getText().toString().trim(),
                                rbMale.isChecked()?Constant.MALE:Constant.FEMALE,
                                Hobby,
                                Utils.getFormatedDateToInserted(etdob.getText().toString().trim()),
                                citylist.get(spcity.getSelectedItemPosition()).getCityId(),
                                languagelist.get(splanguage.getSelectedItemPosition()).getLanguageId(),
                                educationlist.get(speducation.getSelectedItemPosition()).getEducationId(),userModel.getIsFavourite(),userModel.getUserId());

                        if (lastinertedid > 0) {
                            Toast.makeText(UserListRegistration.this, "User Updated Successfully", Toast.LENGTH_SHORT).show();
                            Intent addCandidateIntent = new Intent(UserListRegistration.this, ActivityUserListByGender.class);
                            startActivity(addCandidateIntent);
                        } else {
                            Toast.makeText(UserListRegistration.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
                        }

                    }

                }
            }
        });
    }

    void setSpinner(){
        citylist.addAll(new TblMstCity(this).getCitylist());
        languagelist.addAll(new TblMstLanguage(this).getLanguagelist());
        educationlist.addAll(new TblMstEducation(this).getEducationlist());

        cityAdapter = new CityAdapter(this,citylist);
        languageAdapter = new LanguageAdapter(this,languagelist);
        educationAdapter = new EducationAdapter(this, educationlist);

        spcity.setAdapter(cityAdapter);
        splanguage.setAdapter(languageAdapter);
        speducation.setAdapter(educationAdapter);

    }

     void setDateToView(){
        final Calendar newCalendar = Calendar.getInstance();
        etdob.setText(String.format("%02d",newCalendar.get(Calendar.DAY_OF_MONTH)) + "/" +
                String.format("%02d",newCalendar.get(Calendar.MONTH)+1)+ "/" +
                newCalendar.get(Calendar.YEAR));
    }

    void viewByRefernce(){
        etFirstname =findViewById(R.id.etActFirstName);
        etMiddlename = findViewById(R.id.etActMiddleName);
        etLastName = findViewById(R.id.etActLastName);
        etMobileNumber = findViewById(R.id.etActMobileNumber);
        etEmail = findViewById(R.id.etActEmailAddress);
        etdob = findViewById(R.id.etActDob);
        spcity = findViewById(R.id.spCity);
        splanguage = findViewById(R.id.spLanguage);
        speducation = findViewById(R.id.spEducation);
        rgGender = findViewById(R.id.rbGender);
        rbFemale = findViewById(R.id.rbFemale);
        rbMale = findViewById(R.id.rbMale);
        cbCricket = findViewById(R.id.cbActCricket);
        cbFootball = findViewById(R.id.cbActFootball);
        cbHockey = findViewById(R.id.cbActHockey);
        cbBasketball = findViewById(R.id.cbActBasketBall);
        btnsubmit = findViewById(R.id.btnActSubmit);
        tvTitle = findViewById(R.id.tvActTitle);
    }

    boolean isValid(){
        boolean flag = true;
        String pattern = "^[a-zA-Z]+";

        if(TextUtils.isEmpty(etFirstname.getText().toString().trim())){
            etFirstname.setError("Please Enter FirstName");
            flag = false;
        }else {

            if(!etFirstname.getText().toString().trim().matches(pattern)){
                etFirstname.setError("Please Enter Valid FirstName");
                flag = false;
            }
        }

        if(TextUtils.isEmpty(etMiddlename.getText().toString().trim())){
            etMiddlename.setError("Please Enter MiddleName");
            flag = false;
        }else {
            if(!etMiddlename.getText().toString().trim().matches(pattern)){
                etMiddlename.setError("Please Enter Valid MiddleName");
                flag = false;
            }
        }

        if(TextUtils.isEmpty(etLastName.getText().toString().trim())){
            etLastName.setError("Please Enter LastName");
            flag = false;
        }else {
            if (!etLastName.getText().toString().trim().matches(pattern)){
                etLastName.setError("Please Enter Valid LastName");
                flag = false;
            }
        }

        if (TextUtils.isEmpty(etMobileNumber.getText().toString().trim())){
            etMobileNumber.setError("Please Enter MobileNumber");
            flag = false;
        }else {
            if (etMobileNumber.getText().toString().trim().length() < 10 ){
                etMobileNumber.setError("Pleas Enter ValidNumber");
                flag = false;
            }
        }

        if (TextUtils.isEmpty(etEmail.getText().toString().trim())){
            etEmail.setError("Please Enter EmailAddresss");
            flag = false;
        }else {
            String EmailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
            if (!etEmail.getText().toString().trim().matches(EmailPattern)){
                etEmail.setError("Please Enter Valid EmailAddress");
                    flag = false;
            }
        }
        if(spcity.getSelectedItemPosition()==0){
                flag = false;
                Toast.makeText(getApplicationContext(),"Please Select City",Toast.LENGTH_LONG).show();
        }
        if (splanguage.getSelectedItemPosition()==0){
            flag = false;
            Toast.makeText(getApplicationContext(),"Please Select Language",Toast.LENGTH_LONG).show();
        }
        if (speducation.getSelectedItemPosition() == 0){
            flag = false;
            Toast.makeText(getApplicationContext(),"Please Select Education",Toast.LENGTH_LONG).show();
        }
        if (!(rbMale.isChecked() || rbFemale.isChecked())) {
            Toast.makeText(getApplicationContext(), "Please Select Gender", Toast.LENGTH_LONG).show();
            flag = false;
        }
        if (!(cbCricket.isChecked() || cbFootball.isChecked() || cbBasketball.isChecked() || cbHockey.isChecked())) {
            Toast.makeText(getApplicationContext(), "Please Selected Any One Hobby", Toast.LENGTH_SHORT).show();
            flag = false;
        }

        return flag;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        etdob.setText(String.format("%02d",dayOfMonth) + "/" + String.format("%02d",month+1)+ "/" + year);
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(UserListRegistration.this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

}
